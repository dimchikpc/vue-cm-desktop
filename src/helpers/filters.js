import BigNumber from 'bignumber.js'

export const priceDirection = (val, oldVal) => val > oldVal ? 'trading-green--text' : val < oldVal ? 'trading-red--text' : 'trading-white--text'
export const percentDirection = (val) => val > 0 ? 'trading-green--text' : val < 0 ? 'trading-red--text' : 'trading-white--text'
export const toFixed = (price, n = 2) => BigNumber(price).toFixed(n)
export const multipliedBy  = (price, n ) => BigNumber(price).multipliedBy(n).toFormat()
export const toFormat = (price) => BigNumber(price).toFormat()
export const toFormatN = (price, n) => BigNumber(price).toFormat(n)
export const percent = (val) => val > 0 ? `+${val}%` : val < 0 ? `${val}%` : `${val}%`
export const prependUSD = (price) => `$${price}`
