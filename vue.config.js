module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    proxy: {
      "/api/v3": {
        target: "https://api.binance.com"
      },
      "/sapi/v1": {
        target: "https://api.binance.com"
      },
      "/auth": {
        target: "http://localhost:3000"
      }
    }
  },
  pwa: {
    themeColor: '#161a1e'
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
          @import "~@/assets/styles/breakpoints.scss";
          @import "~@/assets/styles/variables.scss";
        `
      },
    }
  }
}
